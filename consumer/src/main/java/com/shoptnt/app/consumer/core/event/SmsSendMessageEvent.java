/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.core.event;


import com.shoptnt.app.core.base.model.vo.SmsSendVO;

/**
 * 发送短信
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午10:26:41
 */
public interface SmsSendMessageEvent {

	/**
	 * 发送短信
	 * @param smsSendVO	短信发送vo
	 */
	void send(SmsSendVO smsSendVO);
}
