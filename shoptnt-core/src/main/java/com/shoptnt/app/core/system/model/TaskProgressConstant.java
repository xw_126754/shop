/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.system.model;

/**
 * @author fk
 * @version v1.0
 * @Description: 任务进度常量类
 * @date 2018/6/25 16:43
 * @since v7.0.0
 */
public class TaskProgressConstant {

    /**
     * 商品索引初始化
     */
    public static final String GOODS_INDEX = "GOODS_INDEX_INIT";

    /**
     * 静态页生成
     */
    public static final String PAGE_CREATE = "PAGE_CREATE";


}
