/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.trade.cart.service.cartbuilder.impl;

import com.shoptnt.app.core.trade.cart.model.vo.CartVO;
import com.shoptnt.app.core.trade.cart.service.cartbuilder.CartShipPriceCalculator;
import com.shoptnt.app.core.trade.order.service.ShippingManager;
import com.shoptnt.app.framework.logs.Logger;
import com.shoptnt.app.framework.logs.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/12/19
 */
@Service
public class CartShipPriceCalculatorImpl implements CartShipPriceCalculator {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ShippingManager shippingManager;

    @Override
    public void countShipPrice(List<CartVO> cartList) {
        shippingManager.setShippingPrice(cartList);
        logger.debug("购物车处理运费结果为：");
        logger.debug(cartList.toString());
    }

}
