/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.system.service;

import com.shoptnt.app.core.system.model.dos.Message;
import com.shoptnt.app.core.system.model.vo.MessageVO;
import com.shoptnt.app.framework.database.Page;

/**
 * 站内消息业务层
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-04 21:50:52
 */
public interface MessageManager {

    /**
     * 查询站内消息列表
     *
     * @param page     页码
     * @param pageSize 每页数量
     * @return Page
     */
    Page list(int page, int pageSize);

    /**
     * 添加站内消息
     *
     * @param messageVO 站内消息
     * @return Message 站内消息
     */
    Message add(MessageVO messageVO);

    /**
     * 通过id查询站内消息
     *
     * @param id 消息id
     * @return 站内消息对象
     */
    Message get(Integer id);
}