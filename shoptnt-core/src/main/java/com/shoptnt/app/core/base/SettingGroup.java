/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.base;

/**
 * 系统设置分组枚举
 * Created by kingapex on 2018/3/19.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/19
 */
public enum SettingGroup {

    /**
     * 系统设置
     */
    SYSTEM,

    /**
     * 站点设置
     */
    SITE,

    /**
     * 商品设置
     */
    GOODS,

    /**
     * 平台联系方式
     */
    INFO,
    /**
     * 商品图片尺寸
     */
    PHOTO_SIZE,
    /**
     * 交易设置
     */
    TRADE,

    /**
     * 积分设置
     */
    POINT,
    /**
     * 分销设置
     */
    DISTRIBUTION,
    /**
     * 测试设置
     */
    TEST,
    /**
     * app推送设置
     */
    PUSH,
    /**
     * page
     */

    PAGE,
    /**
     * ES_SIGN 分词词库秘钥
     */
    ES_SIGN;

}
