/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.seller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;


/**
 * Created by kingapex on 2018/3/10.
 *
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/3/10
 */
@SpringBootApplication
@ComponentScan({"com.shoptnt.app","com.chinapay.secss.util"})
@ServletComponentScan
public class SellerApiApplication {
    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SellerApiApplication.class, args);


    }

}
