/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.deploy.service;

/**
 * 部署执行接口
 * @author kingapex
 * @version 1.0
 * @since 7.0.0
 * 2018/5/14
 */
public interface DeployExecutor {


   /**
    * 执行部署
    * @param deployId 部署id
    */
   void deploy(Integer deployId);

   /**
    * 定义类型
    * @return
    */
   String getType();

}
